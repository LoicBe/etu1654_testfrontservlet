<%-- 
    Document   : valinyPoste
    Created on : 9 nov. 2022, 00:33:44
    Author     : LoicRaveloarimanana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World valiny!</h1>
        <p>nom:<% out.println( request.getAttribute("nom").toString()); %></p>
        <p>prenom:<% out.println( request.getAttribute("prenom").toString()); %></p>
        <p>age:<% out.println( request.getAttribute("age").toString()); %></p>
        <p>dept:<% out.println( request.getAttribute("idDept")); %></p>
        <a href="inserte.doo">retour</a>
    </body>
</html>
