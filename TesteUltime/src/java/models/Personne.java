/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import annotation.MethodeAnnotation;
import utilities.ModelView;

/**
 *
 * @author LoicRaveloarimanana
 */
public class Personne {

    private String nom;
    private String prenom;
    private Integer age;
    private Dept idDept;

    public Dept getIdDept() {
        return idDept;
    }

    public void setIdDept(Dept idDept) {
        this.idDept = idDept;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Personne() {
    }

    @MethodeAnnotation(url = "getAll.doo")
    public void getAll() {
        System.out.println("hehe");
    }

    @MethodeAnnotation(url = "teste.doo")
    public void teste() {

    }

    @MethodeAnnotation(url = "farany.doo")
    public void farany() {

    }

    @MethodeAnnotation(url = "blabla.doo")
    public void faranySisa() {

    }

    @MethodeAnnotation(url = "fonctionTste.doo")
    public ModelView testeHaha() {
        ModelView reponse = new ModelView();
        reponse.setJspFile("teste.jsp");
        String nom = "RAVELOARIMANANA";
        String prenom = "Loic";
        int age = 20;

        reponse.setAttribute("nom", nom);
        reponse.setAttribute("prenom", prenom);
        reponse.setAttribute("age", age);

        return reponse;

    }

    @MethodeAnnotation(url = "inserte.doo")
    public ModelView loadPage() {
        ModelView reponse = new ModelView();
        reponse.setJspFile("poste.jsp");
        Dept[] listeDept = new Dept[2];
        listeDept[0] = new Dept("dept-1", "directeur");
        listeDept[1] = new Dept("dept-2", "secretaire");
        reponse.setAttribute("listeDept", listeDept);

        return reponse;
    }

    @MethodeAnnotation(url = "andrana.doo")
    public ModelView save() {
        ModelView reponse = new ModelView();
        System.out.println(this.nom + "nom lesy ");
        System.out.println(this.prenom + " prenom");
        // reponse.setAttribute("idDept", idDept.getIdDept());

        if (this.age < 18) {
            reponse = loadPage();
            System.out.println(reponse.getJspFile() + "ito");
            reponse.setAttribute("error", "tsy apmy taona");

        } else {
            reponse.setJspFile("valinyPoste.jsp");
            reponse.setAttribute("nom", nom);
            reponse.setAttribute("prenom", prenom);
            reponse.setAttribute("age", age);
            reponse.setAttribute("idDept", idDept.getIdDept());
        }
        return reponse;

    }

}
