/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author LoicRaveloarimanana
 */
public class Dept {
    
    private String idDept;
    private String nomDept;

    public String getIdDept() {
        return idDept;
    }

    public void setIdDept(String idDept) {
        this.idDept = idDept;
    }

    public String getNomDept() {
        return nomDept;
    }

    public void setNomDept(String nomDept) {
        this.nomDept = nomDept;
    }


    public Dept(){}
    
    public Dept(String dept){
        this.idDept = dept;
    }
    
    public Dept(String id , String nom){
        this.idDept = id;
        this.nomDept = nom;
    }
    
}
